import java.util.Scanner;

public class Pr6t1 {
    public static void main(String[] args) {
        int x1, x2;
        int gcd = 0;
        int scm;
        x1 = input();
        x2 = input();
        gcd = gcd(x1, x2, gcd);
        System.out.println("Greatest common divisor is " + gcd);
        scm = scm(x1, x2, gcd);
        System.out.println("Smaller common multiple is " + scm);
    }

    public static int input() {
        int value;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your value");
        while (!scan.hasNextInt()) {
            scan.nextLine();
        }
        value = scan.nextInt();
        return value;
    }

    public static int gcd(int x1, int x2, int gcd) {
        for (int i = 1; i <= x1 && i <= x2; i++) {
            if (x1 % i == 0 && x2 % i == 0) {
                gcd = i;
            }
        }
        return gcd;
    }

    public static int scm(int x1, int x2, int gcd) {
        int scm = (x1 * x2) / gcd;

        return scm;
    }

}
